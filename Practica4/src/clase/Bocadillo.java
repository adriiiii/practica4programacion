package clase;

/**
 *
 * @author Adrian Rodriguez Escalona
 *
 */
public class Bocadillo {
	private String nombre;
	private double precio;
	private double tamano;
	private String tipoPan;

	/**
	 * Es el constructor de bocadillo, hace que se inicialize el objeto creado de
	 * una clase
	 *
	 * @param nombre  del bocadillo
	 * @param precio  del bocadillo
	 * @param tamano  del bocadillo
	 * @param tipoPan del bocadillo
	 */
	public Bocadillo(String nombre, double precio, double tamano, String tipoPan) {
		this.nombre = nombre;
		this.precio = precio;
		this.tamano = tamano;
		this.tipoPan = tipoPan;
	}

	/**
	 * Obtiene el nombre del Bocadillo
	 *
	 * @return del nombre del bocadillo
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Le da un valor al nombre del Bocadillo
	 *
	 * @param nombre del bocadillo
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Obtiene el precio del Bocadillo
	 *
	 * @return del precio del bocadillo
	 */
	public double getPrecio() {
		return precio;
	}

	/**
	 * Le da un valor al precio del Bocadillo
	 *
	 * @param precio del bocadillo
	 */
	public void setPrecio(double precio) {
		this.precio = precio;
	}

	/**
	 * Obtiene el tamano del Bocadillo
	 *
	 * @return del tamano del bocadillo
	 */
	public double getTamano() {
		return tamano;
	}

	/**
	 * Le da un valor al tamano del Bocadillo
	 *
	 * @param tamano del bocadillo
	 */
	public void setTamano(double tamano) {
		this.tamano = tamano;
	}

	/**
	 * Obtiene el tipo de pan del Bocadillo
	 *
	 * @return del tipo de pan del bocadillo
	 */
	public String getTipoPan() {
		return tipoPan;
	}

	/**
	 * Le da un valor al tipo de pan del Bocadillo
	 *
	 * @param tipoPan del bocadillo
	 */
	public void setTipoPan(String tipoPan) {
		this.tipoPan = tipoPan;
	}

	@Override
	public String toString() {
		return "Bocadillo [nombre=" + nombre + ", precio=" + precio + ", tamano=" + tamano + ", tipoPan=" + tipoPan
				+ "]";
	}

}