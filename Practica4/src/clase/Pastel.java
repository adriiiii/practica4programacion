package clase;

/**
 *
 * @author Adrian Rodriguez Escalona
 *
 */
public class Pastel {
	private String nombre;
	private double precio;
	private double tamano;

	/**
	 * Es el constructor de Pastel, hace que se inicialize el objeto creado de una
	 * clase
	 *
	 * @param nombre del pastel
	 * @param precio del pastel
	 * @param tamano del pastel
	 */

	public Pastel(String nombre, double precio, double tamano) {
		this.nombre = nombre;
		this.precio = precio;
		this.tamano = tamano;
	}

	/**
	 * Obtiene el nombre del Pastel
	 *
	 * @return del nombre del pastel
	 */

	public String getNombre() {
		return nombre;
	}

	/**
	 * Le da un valor al nombre del Pastel
	 *
	 * @param nombre del pastel
	 */

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Obtiene el precio del Pastel
	 *
	 * @return del precio del pastel
	 */

	public double getPrecio() {
		return precio;
	}

	/**
	 * Le da un valor al precio del Pastel
	 *
	 * @param precio del pastel
	 */

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	/**
	 * Obtiene el tamano del Pastel
	 *
	 * @return del tamano del pastel
	 */

	public double gettamano() {
		return tamano;
	}

	/**
	 * Le da un valor al tamano del Pastel
	 *
	 * @param tamano del pastel
	 */

	public void settamano(double tamano) {
		this.tamano = tamano;
	}

	@Override
	public String toString() {
		return "Pastel [nombre=" + nombre + ", precio=" + precio + ", tamano=" + tamano + "]";
	}

}