package clase;

/**
 *
 * @author Adrian Rodriguez Escalona
 *
 */
public class Pasteleria {
	Pastel[] pastel;
	Bocadillo[] bocadillo;

	/**
	 * Es para luego poder pedir la cantidad de bocadillos y pasteles quieres que
	 * salgan
	 * 
	 * @param maxPasteles   (maximo de pasteles)
	 * @param maxBocadillos (maximo de bocadillos)
	 */
	public Pasteleria(int maxPasteles, int maxBocadillos) {
		this.pastel = new Pastel[maxPasteles];
		this.bocadillo = new Bocadillo[maxBocadillos];
	}

	/**
	 * Es un metodo para a�adir un pastel con su nombre, precio y tama�o
	 * 
	 * @param nombre del pastel y bocadillo
	 * @param precio del pastel y bocadillo
	 * @param tamano del pastel y bocadillo
	 */
	public void altaPasteles(String nombre, double precio, double tamano) {
		for (int i = 0; i < pastel.length; i++) {
			if (pastel[i] == null) {
				pastel[i] = new Pastel(nombre, precio, tamano);
				break;

			}

		}
	}

	/**
	 * Este metodo sirve para listar los pasteles creados
	 */
	public void listarPasteles() {
		for (int i = 0; i < pastel.length; i++) {
			if (pastel[i] != null) {
				System.out.println(pastel[i]);

			}

		}
	}

	/**
	 * Este metodo sirve para buscar pasteles por nombre
	 * 
	 * @param nombre del pastel
	 * @return pastel[i]
	 */
	public Pastel buscarPasteles(String nombre) {
		for (int i = 0; i < pastel.length; i++) {
			if (pastel[i] != null) {
				if (pastel[i].getNombre().equals(nombre)) {
					return pastel[i];
				}

			}

		}

		return null;
	}

	/**
	 * Este metodo sirve para eliminar un pastel
	 * 
	 * @param nombre del pastel
	 */
	public void eliminarPasteles(String nombre) {
		for (int i = 0; i < pastel.length; i++) {
			if (pastel[i] != null) {
				if (pastel[i].getNombre().equals(nombre)) {
					pastel[i] = null;
				}
			}
		}
	}

	/**
	 * Este metodo sirve para cambiar dos atributos
	 * 
	 * @param precio del pastel
	 * @param nombre del pastel
	 */
	public void cambiarPasteles(double precio, String nombre) {
		for (int i = 0; i < pastel.length; i++) {
			if (pastel[i] != null) {
				if (pastel[i].getNombre().equals(nombre)) {
					pastel[i].setPrecio(precio);
				}
			}
		}

	}

	/**
	 * Este metodo sirve para listar por un atributo
	 * 
	 * @param precio del pastel
	 */
	public void listarAtributo(double precio) {
		for (int i = 0; i < pastel.length; i++) {
			if (pastel[i] != null) {
				if (pastel[i].getPrecio() == precio) {
					System.out.println(pastel[i]);
				}
			}
		}

	}

	/**
	 * Este metodo sirve para mostrar el total del precio de los pasteles
	 */
	public void totalPrecio() {
		int precioTotal = 0;
		for (int i = 0; i < pastel.length; i++) {
			if (pastel[i] != null) {
				precioTotal += pastel[i].getPrecio();

			}

		}
		System.out.println("El precio total de los precios de los pasteles es de: " + precioTotal);
	}

	/**
	 * Es un metodo para a�adir un bocadillo con su nombre, precio, tama�o y tipo de
	 * pan
	 * 
	 * @param nombre  del bocadillo
	 * @param precio  del bocadillo
	 * @param tamano  del bocadillo
	 * @param tipoPan del bocadillo
	 */
	public void altaBocadillos(String nombre, double precio, double tamano, String tipoPan) {
		for (int i = 0; i < bocadillo.length; i++) {
			if (bocadillo[i] == null) {
				bocadillo[i] = new Bocadillo(nombre, precio, tamano, tipoPan);
				break;

			}

		}
	}

	/**
	 * Este metodo sirve para listar los bocadillos creados
	 */
	public void listarBocadillos() {
		for (int i = 0; i < bocadillo.length; i++) {
			if (bocadillo[i] != null) {
				System.out.println(bocadillo[i]);

			}

		}
	}

	/**
	 * Este metodo sirve para buscar bocadillos por el nombre
	 * 
	 * @param nombre del bocadillo
	 * @return bocadillo[i]
	 */
	public Bocadillo buscarBocadillos(String nombre) {
		for (int i = 0; i < bocadillo.length; i++) {
			if (bocadillo[i] != null) {
				if (bocadillo[i].getNombre().equals(nombre)) {
					return bocadillo[i];
				}

			}

		}

		return null;
	}

	/**
	 * Este metodo sirve para eliminar bocadillos
	 * 
	 * @param nombre del bocadillo
	 */
	public void eliminarBocadillos(String nombre) {
		for (int i = 0; i < bocadillo.length; i++) {
			if (bocadillo[i] != null) {
				if (bocadillo[i].getNombre().equals(nombre)) {
					bocadillo[i] = null;
				}
			}
		}
	}

	/**
	 * Este metodo sirve para cambiar atributos de bocadillos
	 * 
	 * @param precio del bocadillo
	 * @param nombre del bocadillo
	 */
	public void cambiarBocadillos(double precio, String nombre) {
		for (int i = 0; i < bocadillo.length; i++) {
			if (bocadillo[i] != null) {
				if (bocadillo[i].getNombre().equals(nombre)) {
					bocadillo[i].setPrecio(precio);
				}
			}
		}

	}

	/**
	 * Este metodo sirve para listar por precio de bocadillos
	 * 
	 * @param precio del bocadillo
	 */
	public void listarPrecioBocadillos(double precio) {
		for (int i = 0; i < bocadillo.length; i++) {
			if (bocadillo[i] != null) {
				if (bocadillo[i].getPrecio() == precio) {
					System.out.println(bocadillo[i]);
				}
			}
		}

	}

	/**
	 * Este metodo sirve para que te muestre el total de precios
	 */
	public void totalPreciosBocadillos() {
		int precioTotal = 0;
		for (int i = 0; i < bocadillo.length; i++) {
			if (bocadillo[i] != null) {
				precioTotal += bocadillo[i].getPrecio();

			}

		}
		System.out.println("El precio total de los precios de los pasteles es de: " + precioTotal);
	}
}
