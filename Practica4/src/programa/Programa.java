package programa;

import java.util.Scanner;

import clase.Bocadillo;
import clase.Pasteleria;

/**
 *
 * @author Adrian Rodriguez Escalona
 *
 */
public class Programa {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int opcion = 0;

		System.out.println("Cuantos pasteles quieres que tenga la pasteleria de La Locura");
		int maxPasteles = input.nextInt();

		System.out.println("Cuantos bocadillos quieres que tenga la pasteleria de La Locura");
		int maxBocadillos = input.nextInt();

		Pasteleria pasteleria = new Pasteleria(maxPasteles, maxBocadillos);

		pasteleria.altaPasteles("Locura de fresa", 15.50, 10.00);
		pasteleria.altaPasteles("Ida de olla de platano", 13.50, 12.00);
		pasteleria.altaPasteles("Demencia de macedonia", 15.50, 10.00);
		pasteleria.altaPasteles("Calaverada mortal de limon", 13.50, 12.00);
		pasteleria.altaPasteles("Frenes� de manzana", 17.50, 15.00);
		pasteleria.altaPasteles("Explosion de menta", 17.80, 15.99);
		pasteleria.altaPasteles("Extrema payaseria de MarianoRajoy", 18.50, 16.00);

		pasteleria.altaBocadillos("Demencia furiosa de jamon", 6.50, 9.00, "Pan de nieve");
		pasteleria.altaBocadillos("Vesania de lomo y queso", 10.50, 9.00, "Pan gallego");
		pasteleria.altaBocadillos("Extravagancia elegante de bacon, queso y huevo", 11.00, 10.00, "Pan gallego");
		pasteleria.altaBocadillos("Enajenacion de fuet", 6.50, 9.00, "Pan de nieve");
		pasteleria.altaBocadillos("Insensatez infrahumana de vegetal", 10.00, 9.00, "Pan de chapata");
		pasteleria.altaBocadillos("Trastorno heavy de chocolate", 6.50, 9.00, "Baguete");
		pasteleria.altaBocadillos("Lunatico brutalico de sandwich de jamon york y queso", 5.50, 6.00,
				"Pan tostado bimbo");
		pasteleria.altaBocadillos("Irracionalidad absurda de PedroSanchez", 11.50, 10.50, "Pan de hamburgesa");

		do {
			System.out.println("~                                                     ~");
			System.out.println("_______________________________________________________");
			System.out.println("| Bienvenido a la pasteleria de La Locura              |");
			System.out.println("|______________________________________________________|");
			System.out.println("| Digame que desea saber seleccionando una opcion:     |");
			System.out.println("| 1- Mostrar metodo alta (pastel)                      |");
			System.out.println("| 2- Mostrar metodo buscar (pastel)                    |");
			System.out.println("| 3- Mostrar metodo eliminar (pastel)                  |");
			System.out.println("| 4- Mostrar metodo listar general (pastel)            |");
			System.out.println("| 5- Mostrar metodo cambiar precio (pastel)            |");
			System.out.println("| 6- Mostrar metodo listar por precio de los pasteles  |");
			System.out.println("| 7- Suma total de los precios (pastel)                |");
			System.out.println("| 8- Mostrar metodo alta (bocadillo)                   |");
			System.out.println("| 9- Mostrar metodo buscar (bocadillo)                 |");
			System.out.println("| 10- Mostrar metodo eliminar (bocadillo)              |");
			System.out.println("| 11- Mostrar metodo listar general (bocadillo)        |");
			System.out.println("| 12- Mostrar metodo cambiar precio (bocadillo)        |");
			System.out.println("| 13- Mostrar metodo listar por precio de bocadillos   |");
			System.out.println("| 14- Suma total de los precios (bocadillo)            |");
			System.out.println("| 15- Salir de la pasteleria de La Locura              |");
			System.out.println("|______________________________________________________|");
			opcion = input.nextInt();
			input.nextLine();

			switch (opcion) {
			case 1:
				System.out.println("Introduce un nombre de un pastel");
				String nombrePastel1 = input.nextLine();

				System.out.println("Introduce un tama�o de un pastel");
				double tamanoPastel = input.nextDouble();

				System.out.println("Introduce un precio de un pastel");
				double precioPastel = input.nextDouble();

				System.out.println(
						"Procedemos a crear su pastel: " + nombrePastel1 + " " + tamanoPastel + " " + precioPastel);
				pasteleria.altaPasteles(nombrePastel1, tamanoPastel, precioPastel);
				System.out.println("Su pastel se ha creado correctamente, gracias por confiar en nosotros");
				break;

			case 2:
				System.out.println("Introduce un nombre de un pastel");
				String nombrePastel2 = input.nextLine();

				System.out.println("Tu pastel es este: " + pasteleria.buscarPasteles(nombrePastel2));
				break;

			case 3:
				System.out.println("Introduce un nombre de un pastel");
				String nombrePastel3 = input.nextLine();

				pasteleria.eliminarPasteles(nombrePastel3);
				System.out.println("Su pastel " + nombrePastel3 + " ha sido eliminado");
				break;

			case 4:
				System.out.println("Procedemos a mostrar todo el catalogo de la pasteleria de La Locura de pasteles");
				pasteleria.listarPasteles();
				break;

			case 5:
				System.out.println("Dame el nombre del pastel que le quieras cambiar el precio");
				String nombrePastel4 = input.nextLine();

				System.out.println("Dame el precio del pastel que quieras cambiar");
				double precioPastel1 = input.nextDouble();

				pasteleria.cambiarPasteles(precioPastel1, nombrePastel4);
				System.out.println("Le has cambiado al pastel: " + nombrePastel4 + " el precio a: " + precioPastel1);
				break;

			case 6:
				System.out.println("Dame un precio para que salga el listado");
				double precioPastel2 = input.nextDouble();

				pasteleria.listarAtributo(precioPastel2);
				System.out.println("Este es el listado relacionado con el precio que has introducido");
				break;

			case 7:
				System.out.println("Mostrando la suma total de los precios de los pasteles");
				pasteleria.totalPrecio();
				break;

			case 8:
				System.out.println("Introduce un nombre de un bocadillo");
				String nombreBocadillo1 = input.nextLine();

				System.out.println("Introduce un tama�o de un bocadillo");
				double tamanoBocadillo = input.nextDouble();

				System.out.println("Introduce un precio de un bocadillo");
				double precioBocadillo = input.nextDouble();

				System.out.println("Introduce un tipo de pan al bocadillo");
				String tipoPanBocadillo = input.nextLine();

				System.out.println("Procedemos a crear su bocadillo: " + nombreBocadillo1 + " " + tamanoBocadillo + " "
						+ precioBocadillo + " " + tipoPanBocadillo);
				pasteleria.altaBocadillos(nombreBocadillo1, tamanoBocadillo, precioBocadillo, tipoPanBocadillo);
				System.out.println("Su pastel se ha creado correctamente, gracias por confiar en nosotros");
				break;

			case 9:
				System.out.println("Introduce un nombre de un bocadillo");
				String nombreBocadillo2 = input.nextLine();

				System.out.println("Tu bocadillo es este: " + pasteleria.buscarBocadillos(nombreBocadillo2));
				break;

			case 10:
				System.out.println("Introduce un nombre de un bocadillo");
				String nombreBocadillo3 = input.nextLine();

				pasteleria.eliminarBocadillos(nombreBocadillo3);
				System.out.println("Su bocadillo " + nombreBocadillo3 + " ha sido eliminado");
				break;

			case 11:
				System.out.println("Procedemos a mostrar todo el catalogo de la pasteleria de La Locura de bocadillos");
				pasteleria.listarBocadillos();
				break;

			case 12:
				System.out.println("Dame el nombre del bocadillo que le quieras cambiar el precio");
				String nombreBocadillo4 = input.nextLine();

				System.out.println("Dame el precio del bocadillo que quieras cambiar");
				double precioBocadillo1 = input.nextDouble();

				pasteleria.cambiarBocadillos(precioBocadillo1, nombreBocadillo4);
				System.out.println(
						"Le has cambiado al bocadillo: " + nombreBocadillo4 + " el precio a: " + precioBocadillo1);
				break;

			case 13:
				System.out.println("Dame un precio para que salga el listado");
				double precioBocadillo2 = input.nextDouble();

				pasteleria.listarPrecioBocadillos(precioBocadillo2);
				System.out.println("Este es el listado relacionado con el precio que has introducido");
				break;

			case 14:
				System.out.println("Mostrando la suma total de los precios");
				pasteleria.totalPreciosBocadillos();
				break;

			case 15:
				System.out.println("Saliendo de la pasteleria");
				System.out.println("Vuelva pronto, le estaremos esperando");
				break;

			default:
				System.out.println("Opcion no valida, vuelva a elegir opcion");
				break;

			}

		} while (opcion != 8);

		input.close();
	}
}